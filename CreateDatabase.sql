create database FuelReports;

go

use FuelReports;

create table PetrolStationReports(
Id uniqueidentifier NOT NULL PRIMARY KEY,
Date datetime2
);

go

create table PetrolStations(
Id uniqueidentifier NOT NULL PRIMARY KEY,
Name nvarchar(max) NULL,
Address nvarchar(max) NULL,
City nvarchar(max) NULL,
PetrolStationReportId uniqueidentifier foreign key references PetrolStationReports(Id)
);

go

create table Fuels(
Id uniqueidentifier NOT NULL PRIMARY KEY,
Price decimal(18,2) NOT NULL,
FuelType nvarchar(max) NULL,
PetrolStationId uniqueidentifier foreign key references PetrolStations(Id)
);