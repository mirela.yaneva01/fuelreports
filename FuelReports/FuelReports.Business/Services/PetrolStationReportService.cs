﻿using FuelReports.Contract.Models;
using FuelReports.Data.Repositories;
using System;
using System.Collections.Generic;

namespace FuelReports.Business.Services
{
    public class PetrolStationReportService
    {
        public void SaveReports(List<PetrolStationReport> reports)
        {
            var reportRepo = new PetrolStationReportRepository();
            var petrolStationRepo = new PetrolStationRepository();
            var fuelRepo = new FuelRepository();

            foreach (var report in reports)
            {
                Guid reportId = reportRepo.Create(report);

                foreach (var petrolStation in report.PetrolStations)
                {
                    petrolStation.PetrolStationReportId = reportId;
                    Guid petrolStationId = petrolStationRepo.Create(petrolStation);

                    foreach (var fuel in petrolStation.Fuels)
                    {
                        fuel.PetrolStationId = petrolStationId;
                        fuelRepo.Create(fuel);
                    }
                }
            }
        }
    }
}
