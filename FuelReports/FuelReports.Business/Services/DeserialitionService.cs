﻿using FuelReports.Contract.Models;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FuelReports.Business.Services
{
    public class DeserialitionService
    {
        public List<PetrolStationReport> ParseXml(string pathToFolder)
        {
            string[] fileEntries = Directory.GetFiles(pathToFolder);
            XmlSerializer serialize = new XmlSerializer(typeof(PetrolStationReport));
            var result = new List<PetrolStationReport>();

            foreach (var pathToFile in fileEntries)
            {
                PetrolStationReport petrolStationReport = (PetrolStationReport)serialize.Deserialize(new XmlTextReader(pathToFile));
                result.Add(petrolStationReport);
            }

            return result;
        }

    }
}
