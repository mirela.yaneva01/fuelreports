﻿using FuelReports.Contract.Enums;
using FuelReports.Contract.Models;
using FuelReports.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuelReports.Business.Services
{
    public class ReportService
    {
        public List<ReportResult> GetReportData(string period, string fuelType, string petrolStation, string city)
        {
            FuelRepository fuelRepository = new FuelRepository();
            int dashCount = period.Count(x => x == '-');

            FuelType? type = null;
            if (!string.IsNullOrEmpty(fuelType))
            {
                type = (FuelType)Enum.Parse(typeof(FuelType), fuelType);
            }

            if (dashCount > 0 && !DateTime.TryParse(period, out DateTime startDate))
                throw new InvalidOperationException("The provided date is invalid.");
            else
                startDate = new DateTime(int.Parse(period), 1, 1);

            var endDate = dashCount switch
            {
                2 => startDate,
                1 => startDate.AddMonths(1).AddDays(-1),
                _ => startDate.AddYears(1).AddDays(-1),
            };

            return fuelRepository.GetFuelReport(startDate, endDate, type, petrolStation, city);
        }
    }
}
