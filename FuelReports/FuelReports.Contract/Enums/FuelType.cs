﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Contract.Enums
{
    public enum FuelType
    {
        Petrol = 0,
        PremiumPetrol = 1,
        Diesel = 2,
        PremiumDiesel = 3,
        LPG = 4,
        CNG = 5

    }
}
