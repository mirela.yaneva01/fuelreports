﻿using System;

namespace FuelReports.Contract.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}
