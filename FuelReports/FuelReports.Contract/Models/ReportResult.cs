﻿using FuelReports.Contract.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Contract.Models
{
    public class ReportResult
    {
        public FuelType FuelType { get; set; }

        public decimal AveragePrice { get; set; }
    }
}
