﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FuelReports.Contract.Models
{
    [XmlRoot(ElementName = "petrolStations")]
    public class PetrolStationReport : BaseModel
    {
        [XmlAttribute(AttributeName = "date")]
        public DateTime Date { get; set; }

        [XmlElement(ElementName = "petrolStation")]
        public List<PetrolStation> PetrolStations { get; set; }
    }
}

