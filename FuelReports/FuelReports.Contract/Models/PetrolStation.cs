﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FuelReports.Contract.Models
{
    public class PetrolStation : BaseModel
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "address")]
        public string Address { get; set; }

        [XmlAttribute(AttributeName = "city")]
        public string City { get; set; }

        [XmlArray("fuels")]
        [XmlArrayItem("fuel")]
        public List<Fuel> Fuels { get; set; }

        public Guid PetrolStationReportId { get; set; }
    }
}
