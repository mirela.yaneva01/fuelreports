﻿using FuelReports.Contract.Enums;
using System;
using System.Xml.Serialization;

namespace FuelReports.Contract.Models
{
    public class Fuel : BaseModel
    {
        [XmlAttribute(AttributeName = "type")]
        public FuelType FuelType { get; set; }

        [XmlElement(ElementName = "price")]
        public string Price { get; set; }

        public Guid PetrolStationId { get; set; }
    }
}
