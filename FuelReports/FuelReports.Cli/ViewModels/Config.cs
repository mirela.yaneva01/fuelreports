﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using FuelReports.Cli.ViewController;

namespace FuelReports.Cli.ViewModels
{
    public class Config : ICommand
    {
        private readonly ConfigController _controller;

        [Parameter("--data-dir")]
        public string DataDir;

        public Config()
        {
            _controller = new ConfigController();
        }

        public void Execute(object parent)
        {
            _controller.SetDataDir(DataDir);
        }
    }
}
