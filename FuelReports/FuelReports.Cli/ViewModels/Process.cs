﻿using Commander.NET.Interfaces;
using FuelReports.Cli.ViewController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Cli.ViewModels
{
    public class Process : ICommand
    {
        private readonly ProcessController _controller;

        public Process()
        {
            _controller = new ProcessController();
        }

        public void Execute(object parent)
        {
            _controller.Process();
        }
    }
}
