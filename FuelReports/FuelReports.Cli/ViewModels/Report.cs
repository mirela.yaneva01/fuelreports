﻿using Commander.NET.Attributes;
using Commander.NET.Interfaces;
using FuelReports.Cli.ViewController;
using FuelReports.Contract.Enums;
using FuelReports.Contract.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Cli.ViewModels
{
    public class Report : ICommand
    {
        private readonly ReportController _controller;

        [Parameter("--period")]
        public string Period;

        [Parameter("--fuel-type", Required = Required.No)]
        public string FuelType;

        [Parameter("--petrol-station", Required = Required.No)]
        public string PetrolStation;

        [Parameter("--city", Required = Required.No)]
        public string City;

        public Report()
        {
            _controller = new ReportController();
        }

        public void Execute(object parent)
        {
            _controller.Report(Period, FuelType, PetrolStation, City);
        }
    }
}
