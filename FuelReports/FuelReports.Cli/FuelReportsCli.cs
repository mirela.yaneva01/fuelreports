﻿using Commander.NET.Attributes;
using FuelReports.Cli.ViewModels;

namespace FuelReports.Cli
{
    public class FuelReportsCli
    {
        [Command("config")]
        public Config Config;

        [Command("process")]
        public Process Process;

        [Command("report")]
        public Report Report;

    }
}
