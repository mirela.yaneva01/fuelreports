﻿using Commander.NET;
namespace FuelReports.Cli
{
    public class Program
    {
        static void Main(string[] args)
        {
            _ = new CommanderParser<FuelReportsCli>().Parse(args);
        }
    }
}
