﻿using FuelReports.Business.Services;
using FuelReports.Contract.Enums;
using FuelReports.Contract.Models;
using FuelReports.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Cli.ViewController
{
    public class ReportController
    {
        public void Report(string period, string fuelType, string petrolStation, string city)
        {
            ReportService reportService = new ReportService();

            List<ReportResult> reportResults = reportService.GetReportData(period, fuelType, petrolStation, city);

            Console.WriteLine($"Report generated for {period}");
            Console.WriteLine();

            foreach (var row in reportResults)
            {
                Console.WriteLine($"{row.FuelType} \t${row.AveragePrice}");
            }
        }
    }
}
