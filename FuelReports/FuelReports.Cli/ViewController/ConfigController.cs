﻿using FuelReports.Contract;
using System.IO;

namespace FuelReports.Cli.ViewController
{
    public class ConfigController
    {
        public void SetDataDir(string dataDir)
        {
            if (!Directory.Exists(dataDir))
            {
                throw new DirectoryNotFoundException();
            }

            ConfigurationHelper.AddUpdateAppSettings("DataDir", dataDir);
        }
    }
}
