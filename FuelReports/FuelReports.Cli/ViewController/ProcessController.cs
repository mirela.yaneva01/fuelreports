﻿using FuelReports.Business.Services;
using FuelReports.Contract;
using System.IO;

namespace FuelReports.Cli.ViewController
{
    public class ProcessController
    {
        public void Process()
        {
            string pathToFolder = ConfigurationHelper.ReadSetting("DataDir");

            SftpService sftpService = new SftpService();
            sftpService.DownloadAll(pathToFolder);

            DeserialitionService deserialitionService = new DeserialitionService();
            var reports = deserialitionService.ParseXml(pathToFolder);

            PetrolStationReportService petrolStationReportService = new PetrolStationReportService();
            petrolStationReportService.SaveReports(reports);
        }
    }
}
