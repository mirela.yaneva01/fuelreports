﻿using AutoMapper;
using FuelReports.Contract.Models;
using FuelReports.Data.Entities;
using System.Globalization;

namespace FuelReports.Data
{
    public static class MapHelper
    {
        public static MapperConfiguration GetMapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Fuel, FuelEntity>()
                .ForMember(
                    dest => dest.Price,
                    opt => opt.MapFrom(src => decimal.Parse(src.Price.Replace("$", ""), CultureInfo.InvariantCulture))
                );
                cfg.CreateMap<FuelEntity, Fuel>();
            });

            return config;
        }
    }
}
