﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelReports.Data.Entities
{
    public class PetrolStationReportEntity : BaseEntity
    {
        public DateTime Date { get; set; }
    }
}
