﻿using FuelReports.Contract.Models;
using System;
using System.Collections.Generic;

namespace FuelReports.Data.Entities
{
    public class PetrolStationEntity : BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public Guid PetrolStationReportId { get; set; }
    }
}
