﻿using FuelReports.Contract.Enums;
using System;

namespace FuelReports.Data.Entities
{
    public class FuelEntity : BaseEntity
    {
        public decimal Price { get; set; }

        public FuelType FuelType { get; set; }

        public Guid PetrolStationId { get; set; }
    }
}
