﻿using FuelReports.Contract;
using FuelReports.Contract.Enums;
using FuelReports.Contract.Models;
using FuelReports.Data.Entities;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;

namespace FuelReports.Data.Repositories
{
    public class FuelRepository : BaseRepository<Fuel, FuelEntity>
    {
        public List<ReportResult> GetFuelReport(DateTime startingDate, DateTime endDate, FuelType? fuelType, string petrolStation, string city)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                connection.Open();

                StringBuilder sb = new StringBuilder();

                sb.Append(
                @"select FuelType,AVG (Price) as 'Avg Price' from Fuels
                inner join PetrolStations on Fuels.PetrolStationId = PetrolStations.Id
                inner join PetrolStationReports on PetrolStations.PetrolStationReportId = PetrolStationReports.Id
                where
                PetrolStationReports.Date between @StartingDate and @EndDate");

                SqlCommand reportCommand = new SqlCommand("", connection);
                reportCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime2);
                reportCommand.Parameters.Add("@EndDate", SqlDbType.DateTime2);
                reportCommand.Parameters["@StartingDate"].Value = startingDate;
                reportCommand.Parameters["@EndDate"].Value = endDate;

                if (fuelType != null)
                {
                    sb.Append(" and FuelType = @FuelType");
                    reportCommand.Parameters.Add("@FuelType", SqlDbType.Int);
                    reportCommand.Parameters["@FuelType"].Value = fuelType;
                }
                if (!string.IsNullOrEmpty(petrolStation))
                {
                    sb.Append(@" and PetrolStations.Name = @PetrolStationName");
                    reportCommand.Parameters.Add("@PetrolStationName", SqlDbType.NVarChar);
                    reportCommand.Parameters["@PetrolStationName"].Value = petrolStation;
                }
                if (!string.IsNullOrEmpty(city))
                {
                    sb.Append(@" and PetrolStations.City = @City");
                    reportCommand.Parameters.Add("@City", SqlDbType.NVarChar);
                    reportCommand.Parameters["@City"].Value = city;
                }

                sb.Append(@" group by FuelType
                            order by FuelType;");
                reportCommand.CommandText = sb.ToString();

                List<ReportResult> reportResults = new List<ReportResult>();

                using (SqlDataReader reader = reportCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReportResult row = new ReportResult();

                        row.FuelType = (FuelType)Enum.Parse(typeof(FuelType), reader.GetFieldValue<string>("FuelType"));
                        row.AveragePrice = reader.GetFieldValue<decimal>("Avg Price");
                        reportResults.Add(row);
                    }
                }
                return reportResults;
            }
        }

        protected override FuelEntity MapReaderToEntity(SqlDataReader reader)
        {
            FuelEntity entity = new FuelEntity();

            entity.FuelType = (FuelType)reader.GetFieldValue<int>("FuelType");
            entity.Price = reader.GetFieldValue<decimal>("Price");
            entity.Id = reader.GetFieldValue<Guid>("Id");
            entity.PetrolStationId = reader.GetFieldValue<Guid>("PetrolStationId");

            return entity;
        }
    }
}
