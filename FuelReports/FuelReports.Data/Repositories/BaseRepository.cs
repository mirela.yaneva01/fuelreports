﻿using AutoMapper;
using FuelReports.Contract;
using FuelReports.Contract.Models;
using FuelReports.Data.Entities;
using Microsoft.Data.SqlClient;
using Pluralize.NET.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace FuelReports.Data.Repositories
{
    public abstract class BaseRepository<T, TEntity>
       where TEntity : BaseEntity, new()
       where T : BaseModel

    {
        private Pluralizer _pluralizer;
        protected Mapper _mapper;

        public BaseRepository()
        {
            _pluralizer = new Pluralizer();
            _mapper = new Mapper(MapHelper.GetMapperConfiguration());
        }

        public Guid Create(T dto)
        {
            var entity = MapDTOToEntity(dto);
            entity.Id = Guid.NewGuid();

            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name.Replace("Entity", string.Empty));

            IEnumerable<string> columns = entityType
            .GetProperties()
            .Select(x => x.Name);

            string columnsJoin = string.Join(",", columns);

            IEnumerable<string> values = columns.Select(x => $"@{x}");
            string valuesJoin = string.Join(",", values);

            string queryString =
                $@"INSERT INTO {tableName} ({columnsJoin})
                    VALUES({valuesJoin})";

            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddRange(GetParameters(entity));

                connection.Open();
                command.ExecuteNonQuery();
            }
            return entity.Id;
        }

        public List<T> Read()
        {
            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name.Replace("Entity", string.Empty));

            string readQueryString =
                $@"SELECT *
                    FROM {tableName}";

            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                SqlCommand command = new SqlCommand(readQueryString, connection);

                connection.Open();

                List<T> result = new List<T>();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TEntity row = MapReaderToEntity(reader);
                        var dto = MapEntityToDTO(row);

                        result.Add(dto);
                    }
                }
                return result;
            }
        }
        
        public T ReadById(Guid id)
        {
            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name.Replace("Entity", string.Empty));

            string readQueryString =
                $@"SELECT *
                    FROM {tableName}
                    WHERE Id = {id}";

            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                SqlCommand command = new SqlCommand(readQueryString, connection);

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TEntity row = MapReaderToEntity(reader);
                        var dto = MapEntityToDTO(row);

                        return dto;
                    }
                }
            }

            return null;
        }

        public void Update(TEntity entity)
        {
            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name.Replace("Entity", string.Empty));

            IEnumerable<string> columns = entityType
            .GetProperties()
            .Select(x => x.Name);

            IEnumerable<string> values = columns.Select(x => $"{x} = @{x}");
            string valuesJoin = string.Join(",", values);

            string updateQueryString =
                $@"UPDATE {tableName}
                    SET {valuesJoin}
                    WHERE Id = {entity.Id}";

            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                SqlCommand updateCommand = new SqlCommand(updateQueryString, connection);
                updateCommand.Parameters.AddRange(GetParameters(entity));

                connection.Open();
                updateCommand.ExecuteNonQuery();
            }
        }

        public void Delete(Guid id)
        {
            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name.Replace("Entity", string.Empty));

            string deleteQueryString =
                $@"DELETE FROM {tableName}
                    WHERE Id = {id}";

            using (SqlConnection connection = new SqlConnection(ConfigurationHelper.ReadSetting("ConnectionString")))
            {
                SqlCommand deleteCommand = new SqlCommand(deleteQueryString, connection);

                connection.Open();
                deleteCommand.ExecuteNonQuery();
            }
        }

        protected virtual TEntity MapDTOToEntity(T dto)
        {
            return _mapper.Map<TEntity>(dto);
        }

        protected virtual T MapEntityToDTO(TEntity entity)
        {
            return _mapper.Map<T>(entity);
        }

        protected virtual TEntity MapReaderToEntity(SqlDataReader reader)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
           .GetProperties();
            TEntity entity = new TEntity();

            foreach (var property in properties)
            {

                if (property.PropertyType.IsEnum)
                {
                    int value = reader.GetFieldValue<int>(property.Name);
                    if (Enum.IsDefined(property.PropertyType, value))
                    {
                        property.SetValue(entity, Enum.ToObject(property.PropertyType, value));
                    }
                }
                else
                {
                    property.SetValue(entity, reader.GetValue(property.Name));
                }
            }

            return entity;
        }

        private SqlParameter[] GetParameters(TEntity entity)
        {

            IEnumerable<PropertyInfo> properties = typeof(TEntity)
            .GetProperties();

            List<SqlParameter> sqlParameters = new List<SqlParameter>();

            foreach (var property in properties)
            {
                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = $"@{property.Name}";
                sqlParameter.Value = property.GetValue(entity);
                sqlParameters.Add(sqlParameter);
            }

            return sqlParameters.ToArray();
        }
    }

}
