﻿using FuelReports.Contract.Models;
using FuelReports.Data.Entities;
using Microsoft.Data.SqlClient;
using System;
using System.Data;

namespace FuelReports.Data.Repositories
{
    public class PetrolStationRepository : BaseRepository<PetrolStation, PetrolStationEntity>
    {
        protected override PetrolStationEntity MapReaderToEntity(SqlDataReader reader)
        {
            PetrolStationEntity entity = new PetrolStationEntity();

            entity.Address = reader.GetFieldValue<string>("Address");
            entity.City = reader.GetFieldValue<string>("City");
            entity.Id = reader.GetFieldValue<Guid>("Id");
            entity.Name = reader.GetFieldValue<string>("Name");
            entity.PetrolStationReportId = reader.GetFieldValue<Guid>("PetrolStationReportId");
            
            return entity;
        }
    }
}
