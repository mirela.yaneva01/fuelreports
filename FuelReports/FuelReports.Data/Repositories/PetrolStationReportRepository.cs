﻿using FuelReports.Contract.Models;
using FuelReports.Data.Entities;
using Microsoft.Data.SqlClient;
using System;
using System.Data;
namespace FuelReports.Data.Repositories
{
    public class PetrolStationReportRepository : BaseRepository<PetrolStationReport, PetrolStationReportEntity>
    {
        protected override PetrolStationReportEntity MapReaderToEntity(SqlDataReader reader)
        {
            PetrolStationReportEntity entity = new PetrolStationReportEntity();

            entity.Date = reader.GetFieldValue<DateTime>("Date");
            entity.Id = reader.GetFieldValue<Guid>("Id");

            return entity;
        }
    }
}
